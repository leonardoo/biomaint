from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'work_order.views.workorder.create',name="index"),
    (r'^form/equip$', 'work_order.views.equip.getform'),
    (r'^form/workorder$', 'work_order.views.workorder.getform'),
    url(r'^view$', 'work_order.views.workorder.view',name="view Order"),
    url(r'^finalize$', 'work_order.views.workorder.finalize',name="finalize Order"),
    url(r'^login$', 'users.views.user.login',name="login"),
    url(r'^logout$', 'users.views.user.logout',name="logout"),
    url(r'^admin/', include('work_order.urls')),
    url(r'^admin/', include('users.urls')),
    url(r'^addcoment/(\d+)$', 'work_order.views.workorder.add_comment'),
    # Examples:
    # url(r'^$', 'biomaint.views.home', name='home'),
    # url(r'^biomaint/', include('biomaint.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
