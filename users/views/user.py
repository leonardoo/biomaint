# Create your views here.
import hashlib
import hmac

from django.core.signing import Signer
from django.shortcuts import render_to_response,redirect
from django.template import RequestContext
from django.utils.translation import ugettext as _

from users.decorators import staff_menber
from users.models import User as Instance
from users.forms import UserForm as Form

title = _('User')

def login(request):
	msg=''
	if request.POST:
		user = Instance.objects.filter(username__exact=request.POST['username'])

		if user.count() > 0:
			user = user[0]
			value = request.POST['pwd']
			if (len(value) > 0) and (user.valid_password(value)):
				request.session['user'] = user.id
				request.session.set_expiry(300)
				if (user.group !=  None) and (str(user.group) == 'Admin'):
					request.session['admin'] = True
					return redirect('view Order')
				return redirect('index')
			else:
				msg = 'El password no coincide con el asociado al usuario'	

		else:
			msg = 'usuario no encontrado'
		

	return render_to_response('biomaint/user/login.html',
                              context_instance=RequestContext(request))

def logout(request):

	request.session.flush()

	return redirect('login')

@staff_menber(Admin=True)
def create(request):
	form = None
	if request.POST:
		form = Form(request.POST)
		if form.is_valid():
			form = form.save()
			form = Form()
	if form == None:
		form = Form()
	return render_to_response('biomaint/estandares/forms.html',
								{'form': form,
								'title': title},
                              context_instance=RequestContext(request))

@staff_menber(Admin=True)
def view(request):
	
	form = [_('User Name'),_('Create at'),_('Email'),_('Last edit'),_('Group')]
	data = []
	obj = Instance.objects.all()
	for i in obj:

		init = [i.username,
				i.create,
				i.email,
				i.edited,
				str(i.group),
				{'id':i.id}]
		data.append( init)
		
	return render_to_response('biomaint/estandares/table_objects.html',
								{'form': form,
								'title': title,
								'data': data,
								'modify': True},
                              context_instance=RequestContext(request))


@staff_menber(Admin=True)
def alter(request,id=0):
	if request.POST:
		form = Form(request.POST)
		if form.is_valid():
			form.save()
			form = Form()
	obj = Instance.objects.filter(id__exact=id)
	if obj.count() > 0:
		data = model_to_dict(obj[0])
		form = Form(initial=data)
		return render_to_response('biomaint/estandares/forms.html',
									{'form': form,
									'title': title},
	                              context_instance=RequestContext(request))
	else:
		return redirect('view all users')