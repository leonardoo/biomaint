from django.conf.urls import patterns, include, url

user_patterns = patterns('users.views.user',
    url(r'^$', 'view',name="view all users"),
    url(r'^create$', 'create',name="create user"),
    #url(r'^modify/(\d+)$', alter),
    #url(r'^delete$', delete),
)

group_patterns = patterns('users.views.group',
    url(r'^$', 'view',name="view all groups"),
    url(r'^create$', 'create',name="create group"),
    #url(r'^modify/(\d+)$', alter),
    #url(r'^delete$', delete),
)

urlpatterns = patterns('',
    url(r'^user/', include(user_patterns)),
    url(r'^group/', include(group_patterns)),
)