import hashlib
import hmac

from django.core.signing import Signer
from django.db import models
from django.utils.translation import ugettext as _

# Create your models here.


class User(models.Model):

    username = models.CharField(max_length=40)
    password = models.TextField()
    create   = models.DateField(auto_now=True)
    email     = models.EmailField()
    edited   = models.DateField(auto_now_add=True)
    group    = models.ForeignKey('Group', null=True,blank=True)

    def __init__(self, *args, **kwargs):
        super(User, self).__init__(*args, **kwargs)

    class Meta:
        verbose_name = _('User')
        verbose_name_plural = _('Users')

    def __str__(self):
        pass

    def save(self, *args, **kwargs):
        super(User, self).save(*args, **kwargs) # Call the "real" save() method.
    
    def set_password(self,password):
        signer = Signer()
        key = signer.sign(password)
        value = password
        password = hmac.new(key, value, hashlib.sha256).hexdigest()
        self.password = password

    def valid_password(self,password):
        signer = Signer()
        key = signer.sign(password)
        value = password
        password = hmac.new( key,value, hashlib.sha256).hexdigest()
        if password == self.password:
            return True

        return False

    @models.permalink
    def get_absolute_url(self):
        return ('')

class Group(models.Model):

    name     = models.CharField(max_length=40)
    description = models.TextField(blank=True)

    class Meta:
        verbose_name = _('Group')
        verbose_name_plural = _('Groups')

    def __str__(self):
        return u"%s" % (self.name)

    

