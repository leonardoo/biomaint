from functools import wraps
from django.shortcuts import redirect
from django.utils.decorators import available_attrs


def staff_menber(Admin = False):
	def decorator(view_func):
		@wraps(view_func, assigned=available_attrs(view_func))		
		def _checklogin(request, *args, **kwargs):

			if ((request.session.get('user') != None) and 
				((request.session.get('admin') == True) or 
				(not Admin))):
				return view_func(request, *args, **kwargs)
			elif request.session.get('user') != None:
				return redirect('index')
			else:
				return redirect('login')

		return _checklogin
	return decorator