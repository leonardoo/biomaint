from django import forms
from django.utils.translation import ugettext as _
from .models import User,Group

class UserForm(forms.ModelForm):

    password1 = forms.CharField(label=_("Password"),
                                widget=forms.PasswordInput)

    class Meta:
        model = User
        include = ('username','email','group')
        exclude = ('password')
        fields = ('username','password1','email','group')

    def save(self, commit=True):
        user = super(UserForm, self).save(commit=False)
        password = self.cleaned_data["password1"]
        if len(password) > 0:
            user.set_password(password)
        if commit:
            user.save()
        return user



class GroupForm(forms.ModelForm):
    class Meta:
        model = Group
    

