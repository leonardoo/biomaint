from django.contrib.formtools.preview import FormPreview
from .models import *
from django import forms

class EquipForm(forms.ModelForm):
    
    class Meta:
        model = Equip
        fields = ('number_active',
                    'equip_name',
					'type_equip',
					'area_equip')

class EquipAreaForm(forms.ModelForm):
    class Meta:
        model = EquipArea
    

class PriorityForm(forms.ModelForm):
    class Meta:
        model = Priority


class StatusForm(forms.ModelForm):
    class Meta:
        model = Status    

class EquipTypeForm(forms.ModelForm):
    class Meta:
        model = EquipType


class WorkOrderForm(forms.ModelForm):
    class Meta:
        model = WorkOrder
        fields = ('description','work_priority')

class RepairDetailForm(forms.ModelForm):
        class Meta:
            model = RepairDetail
            fields = ('workorder','description')
            

class SomeModelFormPreview(FormPreview):
    def done(self, request, cleaned_data):
        # Do something with the cleaned_data, then redirect
        # to a "success" page.
        return HttpResponseRedirect('/form/success')
        
