 # -*- coding: UTF-8 -*-

from django.db import models
from django.utils.translation import ugettext as _
# Create your models here.


class WorkOrder(models.Model):

    create_at       = models.DateField(auto_now=True)
    date_resolved   = models.DateField(null=True)
    description     = models.TextField(blank=False,
                                        verbose_name='Descripción',
                                        help_text="Detalle brevemente qué problema presenta el equipo")
    finalized       = models.BooleanField(default=False,
                                            verbose_name="Resuelta")
    equip_to_repair = models.ForeignKey('Equip',verbose_name="Equipo")
    reported_by     = models.ForeignKey('users.User',related_name='wo+',
                                            verbose_name="Reportado por")
    resolved_by     = models.ForeignKey('users.User',related_name='wo+',
                                            null=True)
    work_priority   = models.ForeignKey('Priority',verbose_name="Prioridad",
                                            help_text="Por favor seleccione la prioridad adecuadamente, no todas las prioridades son urgentes.")
    work_status     = models.ForeignKey('Status',verbose_name="Estado")

    comment         = models.ManyToManyField('RepairDetail',verbose_name=_("Comment"),
                                            related_name='wo+')

    class Meta:
        verbose_name = _('WorkOrder')
        verbose_name_plural = _('WorkOrders')

    def __str__(self):
        return u"%s" % (self.id)

    def finalize(self):
        self.finalized = True

class Equip(models.Model):

    equip_name      = models.CharField(max_length=50,
                                        verbose_name='Nombre del Equipo',
                                        help_text="¿Qué equipo es? Ej.: Monitor de signos vitales")
    number_active   = models.PositiveIntegerField(unique=True,
                                        verbose_name='Numero de Activo',
                                        help_text="Coloque el número del activo del equipo")
    type_equip      = models.ForeignKey('EquipType',
                                        verbose_name='Tipo de Equipo',
                                        help_text="Ej.: aires acondicionados, si es un aire el que está fallando.")
    area_equip      = models.ForeignKey('EquipArea',
                                        verbose_name='Area',
                                        help_text="Seleccione el área de la lista")

    class Meta:
        verbose_name = _('Equip')
        verbose_name_plural = _('Equips')

    def __str__(self):
        return u"%s - %s" % (self.number_active,self.equip_name)

    def __unicode__(self):
        return u"%s - %s" % (self.number_active,self.equip_name)

class EquipType(models.Model):

    name            = models.CharField(max_length=50)
    description     = models.TextField(blank=True)
    class Meta:
        verbose_name = _('EquipType')
        verbose_name_plural = _('EquipTypes')

    def __str__(self):
        return u"%s" % (self.name)

    def __unicode__(self):
        return u"%s" % (self.name)        

class EquipArea(models.Model):

    name            = models.CharField(max_length=50)
    description     = models.TextField(blank=True)
    class Meta:
        verbose_name = _('EquipArea')
        verbose_name_plural = _('EquipAreas')

    def __str__(self):
        return u"%s" % (self.name)

    def __unicode__(self):
        return u"%s" % (self.name)

class Priority(models.Model):

    name            = models.CharField(max_length=50)
    description     = models.TextField(blank=True)
    color           = models.CharField(max_length=50,blank=True)

    class Meta:
        verbose_name = _('priority')
        verbose_name_plural = _('priorities')

    def __str__(self):
        return u"%s" % (self.name)

    def __unicode__(self):
        return u"%s" % (self.name)

class Status(models.Model):

    name            = models.CharField(max_length=50)
    description     = models.TextField(blank=True)
    color           = models.CharField(max_length=50,blank=True)

    class Meta:
        verbose_name = _('Status')
        verbose_name_plural = _('Statuses')

    def __str__(self):
        return u"%s" % (self.name)

    def __unicode__(self):
        return u"%s" % (self.name)

class RepairDetail(models.Model):

    description     = models.TextField()
    by              = models.ForeignKey('users.User',related_name='rd+')
    date_add        = models.DateField(auto_now=True)
    workorder       = models.ForeignKey('WorkOrder',related_name='rd+')
    class Meta:
        verbose_name = _('RepairDetail')
        verbose_name_plural = _('RepairDetails')

    def __str__(self):
        pass
    