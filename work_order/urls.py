from django.conf.urls import patterns, include, url

equip_patterns = patterns('work_order.views.equip',
    url(r'^$', 'view',name="view all equip"),
    url(r'^create$', 'create',name="create equip"),
    url(r'^modify/(\d+)$', 'alter'),
    url(r'^delete$', 'delete'),
)

status_patterns = patterns('work_order.views.status',
    url(r'^$', 'view',name="view all status"),
    url(r'^create$', 'create',name="create status"),
    url(r'^modify/(\d+)$', 'alter'),
    url(r'^delete$', 'delete'),
)

area_patterns = patterns('work_order.views.equiparea',
    url(r'^$', 'view',name="view all area"),
    url(r'^create$', 'create',name="create area"),
    url(r'^modify/(\d+)$', 'alter'),
    url(r'^delete$', 'delete'),
)

type_patterns = patterns('work_order.views.equiptype',
    url(r'^$', 'view',name="view all type"),
    url(r'^create$', 'create',name="create type"),
    url(r'^modify/(\d+)$', 'alter'),
    url(r'^delete$', 'delete'),
)

priority_patterns = patterns('work_order.views.priority',
    url(r'^$', 'view',name="view all priority"),
    url(r'^create$', 'create',name="create priority"),
    url(r'^modify/(\d+)$', 'alter'),
    url(r'^delete$', 'delete'),
)

urlpatterns = patterns('',
    url(r'^status/', include(status_patterns)),
    url(r'^equip/', include(equip_patterns)),
    url(r'^area/', include(area_patterns)),
    url(r'^type/', include(type_patterns)),
    url(r'^priority/', include(priority_patterns)),

)