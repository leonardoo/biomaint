from django import template

register = template.Library()

@register.filter
def isdict(value):
	return isinstance(value, dict)

@register.filter
def getid(obj):
	for i in obj:
		if isinstance(i, dict):
			return i['id']