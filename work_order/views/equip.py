from django.forms.models import modelform_factory
from django.forms.models import model_to_dict
from django.shortcuts import render_to_response
from django.shortcuts import redirect
from django.template import RequestContext

from users.decorators import staff_menber
from work_order.forms import EquipForm as Form
from work_order.models import Equip as Instance

from django.utils.translation import ugettext as _

title = _('Equip')

@staff_menber(Admin=True)
def create(request):
	form = None
	if request.POST:
		form = Form(request.POST)
		if form.is_valid():
			form.save()
			form = Form()
	if form == None:
		form = Form()
	return render_to_response('biomaint/estandares/forms.html',
								{'form': form,
								'title': title},
                              context_instance=RequestContext(request))

@staff_menber(Admin=True)
def alter(request,id=0):
	if request.POST:
		form = Form(request.POST)
		if form.is_valid():
			form.save()
			form = Form()
	obj = Instance.objects.filter(id__exact=id)
	if obj.count() > 0:
		data = model_to_dict(obj[0])
		form = Form(initial=data)
		return render_to_response('biomaint/estandares/forms.html',
									{'form': form,
									'title': title},
	                              context_instance=RequestContext(request))
	else:
		return redirect('view all equip')

def delete(request):
	pass

@staff_menber()
def getform(request):

	form = None
	if request.POST:
		id = request.POST['number_active']
		if id.isdigit():
			obj = Instance.objects.filter(number_active__exact=id)
			data = None
			if obj.count() > 0:
				data = model_to_dict(obj[0])
				print data
			else:
				data = {"number_active":id}

			form = Form(initial=data)

	if form == None:
		form = Form()

	return render_to_response('biomaint/form.html',
                          	  {'form': form},
                              context_instance=RequestContext(request))

@staff_menber( Admin = True)
def view(request):
	
	form = [_('Number Active'),_('Equip name'),_('Equip Type'),_('Equip Area')]
	data = []
	obj = Instance.objects.all()
	for i in obj:
		print i.id
		init = [i.number_active,
				i.equip_name,
				i.type_equip.name,
				i.area_equip.name,
				{'id':i.id}]
		data.append( init)
		
	return render_to_response('biomaint/estandares/table_objects.html',
								{'form': form,
								'title': title,
								'data': data,
								'modify': True},
                              context_instance=RequestContext(request))