from django.core.mail import EmailMessage,EmailMultiAlternatives
from django.db import transaction
from django.forms.models import modelform_factory
from django.forms.models import model_to_dict	
from django.shortcuts import render_to_response
from django.shortcuts import redirect
from django.template import RequestContext
from django.utils.translation import ugettext as _

from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from django.template import Context

from users.models import *
from users.decorators import staff_menber

from work_order.models import *
from work_order.forms import WorkOrderForm,EquipForm,RepairDetailForm

title = _('Work Orders')

def alter(request):
	pass

@transaction.commit_on_success
@staff_menber()
def create(request):
	msg = ''
	if request.POST:
		equips = Equip.objects.filter(number_active__exact=request.POST["number_active"])
		equip = None
		if equips.count() == 0:
			print "no find"
			form_equip = EquipForm(request.POST)
			if form_equip.is_valid():
				equip = form_equip.save()
				print "save equip"
			else:
				msg = 'ha ocurrido un error en la generacion'
				msg += ' de la orden de trabajo favor verificar lo datos'
		else:
			print "find"
			equip = equips[0]


		status = Status.objects.get(pk=1)
		priority = Priority.objects.get(pk=request.POST['work_priority'])

		wo = WorkOrder()
		wo.equip_to_repair = equip
		wo.work_status = status
		wo.work_priority = priority
		if len(request.POST['description']) > 0:
			wo.description = request.POST['description']
		wo.reported_by = User.objects.get(pk=request.session['user'])

		wo.save()
		
	return render_to_response('biomaint/orden.html',
                              context_instance=RequestContext(request))

def delete(request):
	pass

def finalize(request):
	if request.POST:
		print request.POST
		obj = WorkOrder.objects.get(pk=request.POST['id'])
		
		htmly     = get_template('mail/template.html')
		data = [(_('Create At'),obj.create_at),
			(_('Description'),obj.description),
			(_('Equip'),str(obj.equip_to_repair)),
			(_('Priority'),str(obj.work_priority))]
		d = Context({ 'data': data})
		subject = 'Orden de trabajo'
		text_content = 'This is an important message.'
		html_content = htmly.render(d)
		msg = EmailMultiAlternatives(subject = subject, body= text_content, to=[obj.reported_by.email])
		msg.attach_alternative(html_content, "text/html")
		msg.send()
		obj.finalize()
		obj.save()
	return redirect('view Order')

@staff_menber()
def view(request):
	
	if not request.session.get('admin'):
		form = [_('Create At'),
				_('Description'),
				_('Equip'),
				_('Priority'),
				_('Status')]
		data = []
		obj = WorkOrder.objects.filter(reported_by__exact=request.session['user']
			).exclude(finalized__exact=True)
		for i in obj:
			
			init = [i.create_at,
					i.description,
					str(i.equip_to_repair),
					str(i.work_priority),
					str(i.work_status),
					{'id':i.id}]
			data.append( init)
			
		return render_to_response('biomaint/estandares/table_objects.html',
									{'form': form,
									'title': title,
									'data': data},
	                              context_instance=RequestContext(request))
	else:
		form = [_('Create At'),
				_('Description'),
				_('Equip'),
				_('Priority'),
				_('Status')]
		data = []
		obj = WorkOrder.objects.exclude(finalized__exact=True)
		for i in obj:

			init = [i.create_at,
					i.description,
					str(i.equip_to_repair),
					str(i.work_priority),
					str(i.work_status),
					{'id':i.id}]
			data.append( init)
			
		return render_to_response('admin/table_objects.html',
									{'form': form,
									'title': title,
									'data': data},
	                              context_instance=RequestContext(request))

@staff_menber()
def getform(request):
	#if request.POST:
	form=WorkOrderForm()
	return render_to_response('biomaint/form.html',
							  {'form': form},
	                          context_instance=RequestContext(request))

@staff_menber(Admin=True)
def add_comment(request,id=0):
	if request.POST:
		#form = Form(request.POST)
		rd = RepairDetail()
		rd.description = request.POST['description']
		rd.by = User.objects.get(pk=request.session['user'])
		rd.workorder = WorkOrder.objects.get(pk=request.POST['workorder'])

		rd.save()

	data = {"workorder":id}
	form = RepairDetailForm(initial=data)

	return render_to_response('biomaint/estandares/forms.html',
									{'form': form,
									'title': title},
	                              context_instance=RequestContext(request))