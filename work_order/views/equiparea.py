from django.forms.models import model_to_dict
from django.shortcuts import render_to_response,redirect
from django.template import RequestContext

from users.decorators import staff_menber

from work_order.models import EquipArea as Instance
from work_order.forms import EquipAreaForm as Form


from django.utils.translation import ugettext as _

title = _('Equip Area')

@staff_menber(Admin=True)
def create(request):
	form = None
	if request.POST:
		form = Form(request.POST)
		if form.is_valid():
			form.save()
			form = Form()
	if form == None:
		form = Form()
	return render_to_response('biomaint/estandares/forms.html',
								{'form': form,
								'title': title},
                              context_instance=RequestContext(request))
@staff_menber(Admin=True)
def alter(request,id=0):
	if request.POST:
		form = Form(request.POST)
		if form.is_valid():
			form.save()
			form = Form()
	obj = Instance.objects.filter(id__exact=id)
	if obj.count() > 0:
		data = model_to_dict(obj[0])
		form = Form(initial=data)
		return render_to_response('biomaint/estandares/forms.html',
									{'form': form,
									'title': title},
	                              context_instance=RequestContext(request))
	else:
		return redirect('view all area')

@staff_menber(Admin=True)
def delete(request):
	pass

@staff_menber(Admin=True)
def view(request):
	
	form = [_('Name'),_('Description')]
	data = []
	obj = Instance.objects.all()
	for i in obj:

		init = [i.name,
				i.description,
				{'id':i.id}]
		data.append( init)
		
	return render_to_response('biomaint/estandares/table_objects.html',
								{'form': form,
								'title': title,
								'data': data,
								'modify': True},
                              context_instance=RequestContext(request))